=== Duena Revival ===
Contributors: Duena Studio, Max le Fou
Requires at least: 5.0
Tested up to: 5.8
Requires PHP: 5.6
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Duena Revival is a project that aims to maintain and update the Wordpress theme Duena. 

== Description ==
Duena is a Bootstrap template designed specifically for personal blogs. Being Bootstrap-powered the template provides lots of opportunities for editing and tuning. Bootstrap is web developer’s ‘mana’ with all those HTML elements, components and other cool stuff. Even though Bootstrap templates are mostly flat, the gradient on the background creates depth effect that makes content blocks float in the air. Red and its hues prevail in the theme design giving it some warmth and passion. There is no need to describe theme’s responsiveness; for sure the template fits perfectly into all browsers of tablets, phones and PCs. And lastly, the template comes stuffed with lots of amazing features that will make your blog stand out. Feel free to try Duena template!

== Frequently Asked Questions ==

= Why this fork of Duena? =
Because this theme hasn't been updated since years and the original devs never replied to my calls. Good thing the theme was under GPL...

== Changelog ==
Please check the CHANGELOG file on a text editor to see the changes.

== Resources ==
* Bootstrap 5 © 2011-2021 The Bootstrap Authors, Twitter, Inc.
* Flexslider 2 © 2012 Tyler Smith, Woothemes
* Magnific Popup © 2016 Dmitry Semenov
* jQuery Mobile Menu © 2013 Unknown?